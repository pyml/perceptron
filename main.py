# -*- coding:Latin-1 -*-
import perceptron, datas

def main():
    # param�tres applications
    nbreiteration,donnees,dimension,alpha,apprentissage=100,[],2,0.001,500

    # Cr�ation des donn�es d'apprentissage
    donnees = datas.aleatoires(apprentissage)
    # Lanceur du perceptron
    perceptron.main(nbreiteration, donnees, dimension, alpha)

main()
