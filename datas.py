# -*- coding:Latin-1 -*-
import perceptron,random

#
# Classe qui permet de g�n�rer les donn�es
# - de l'apprentissage de mani�re al�atoire
# - de l'apprentissage de mani�re fixe
# - initiales du vecteur de poids al�atoirement
# - initiales du vecteur de poids � z�ros

# alimente les donn�es pour le perceptron
def aleatoires(apprentissage):
    # donn�es du perceptron
    datas = []
    for i in range(apprentissage):
        x1 = random.uniform(0, 1)
        x2 = random.uniform(0, 1)
        datas.append([x1, x2, perceptron.fonction(x1, x2)])
        i += 1
    return datas

# alimente avec des donn�es simples pour le perceptron
def simple():
    return [[1,1,-1],[2,1,-1],[1,2,-1],[2,2,-1],[1,3,-1],[2,3,-1],[4,1,1], [5,1,1], [4,2,1], [5,2,1], [4,3,1], [5,3,1]]


# vecteur de poids
def weights(dimension):
    weights = []
    for i in range(dimension):
        weights.append(random.uniform(0,1))
        i = i + 1
    return weights


# vecteur de poids
def weights_zeros(dimension):
    weights = []
    for i in range(dimension):
        weights.append(0)
        i = i + 1
    return weights