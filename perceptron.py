# -*- coding:Latin-1 -*-
from __future__ import division
import random,datas,plot

# fonction principale
# Note : tableau contiendra les donn�es n�cessaires au trac� de l'hyperplan ( plan dans notre cas )
#   x2 = - (w1/w2)x1 - ?/w2
def main(nbreiteration,datas,dimension,alpha):
    neurone = init(dimension)
    tableau = []
    i,j,nbre = 0,0,len(datas)
    for i in range(nbreiteration):
        erreurs = 0
        for j in range(nbre):
            prediction = sortie(neurone,datas[j])
            neurone = maj(neurone, datas[j], prediction, alpha, dimension)
            if prediction!=datas[j][dimension]:
                erreurs += 1
        if erreurs > 0:
            tableau.append([-(neurone[2][0]/neurone[2][1]), - (neurone[0]/neurone[2][1]) + 2,erreurs])
    plot.maindatas(datas, neurone,tableau)
    return neurone

def fonction(x, y):
    if x+y-1 > 0:
        return 1
    else :
        return -1

def init(dimension):
        return [0.5,0,datas.weights(dimension)]


def init_zeros_weights(dimension):
    return [0.5, 0, datas.weights_zeros(dimension)]

def sortie(neurone,datas):
    i,calc = 0, 0.0
    bias = float(neurone[0])
    vecteur_poids = neurone[2]
    nbre_datas = len(vecteur_poids)
    # on somme les Wi * Xi
    for i in range(nbre_datas):
        calc += float(datas[i]) * float(vecteur_poids[i])

    # on soustrait le biais
    calc -= bias

    # on renvoie 1 ou -1 en fonction de calc
    if calc > 0:
        return 1
    else :
        return -1


# neurone a la structure suivante : [ BIAS OUT VECTEUR_DE_POIDS]
# data a la structure suivante : [ VALEUR_DIMENSION_1 VALEUR_DIMENSION_2 ... VALEUR_DIMENSION_N ETIQUETTE]
# prediction est la valeur calcul� avant la maj du neurone
# alpha est le facteur d'apprentissage
#
# A noter que data[dimension] repr�sente l'�tiquette

def maj(neurone,datas,prediction,alpha,dimension):
    i = 0
    etiquette = datas[dimension]
    bias = neurone[0]
    newweight  = []
    newbiais = ( bias + alpha * ( etiquette - prediction) * -0.5)
    for i in range(dimension):
        w = neurone[2][i] + alpha * ( etiquette - prediction ) * datas[i]
        newweight.append(w)
    return [newbiais,
            neurone[1],
            newweight]

